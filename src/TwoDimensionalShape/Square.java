package TwoDimensionalShape;


import ShapePackage.Shape;

public class Square extends Shape {

    public double lengthOfSide;

    public Square(double lengthOfSide) {
        this.lengthOfSide = lengthOfSide;


    }

    public double perimeter(){
        return lengthOfSide*4;

    }


    public double area(){
        return lengthOfSide*lengthOfSide;
    }

    @Override
    public String toString(){
        return "side Of Square= "+lengthOfSide;
    }

}