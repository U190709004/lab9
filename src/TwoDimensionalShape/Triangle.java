package TwoDimensionalShape;

import ShapePackage.Shape;

public class Triangle extends Shape {


    public double lenghtofBase;
    public double lengthOfTheTriangle2;
    public double lengthOfTheTriangle3;
    public double heightOfTheTriangle;

    public Triangle(double lenghtofBase, double lengthOfTheTriangle2, double lengthOfTheTriangle3, double heightOfTheTriangle) {
        this.lenghtofBase = lenghtofBase;
        this.lengthOfTheTriangle2 = lengthOfTheTriangle2;
        this.lengthOfTheTriangle3 = lengthOfTheTriangle3;
        this.heightOfTheTriangle = heightOfTheTriangle;

    }

    public double perimeter() {
        return lenghtofBase+lengthOfTheTriangle2+lengthOfTheTriangle3 ;

    }
    public double area(){
        return (lenghtofBase*heightOfTheTriangle)/2;
    }


    @Override
    public String toString() {
        return "triangle    {" +
                "lenght Of Base= " + lenghtofBase +
                ", length Of TheTriangle2= " + lengthOfTheTriangle2 +
                ", length Of TheTriangle3= " + lengthOfTheTriangle3 +
                ", height Of TheTriangle= " + heightOfTheTriangle +
                '}';
    }
}