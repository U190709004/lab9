package ThreeDimensionalShape;


import TwoDimensionalShape.Square;

public class Cube extends Square {

    public Cube(double lengthOfSide) {
        super(lengthOfSide);
    }


    public double area(){
        return super.area()*6;
    }
    public double perimeter() {
        return (super.perimeter() * 3);
    }

    public double volume()
    {
        return (super.area()*lengthOfSide);
    }

    @Override
    public String toString(){
        return "side Of Cube= "+lengthOfSide;
    }


}