package ThreeDimensionalShape;

import ShapePackage.Shape;

public class Tetrahedron extends Shape {

    public double side;

    public Tetrahedron(double side) {
        this.side = side;
    }

    public double area(){
        return side*side*Math.sqrt(3);

    }

    public double volume(){
        return side*side*side*Math.sqrt(2)/12;
    }

    public double perimeter(){
        return side*6;
    }

    @Override
    public String toString() {
        return "side Of Tetrahedron= " + side;
    }
}