package TwoDimensionalShape;

import ShapePackage.Shape;

public class Circle extends Shape {

    public double radius;

    public Circle(double radius) {
        this.radius = radius;
    }
    public double perimeter(){
        return Math.PI*2*radius;

    }

    public double area(){
        return Math.PI*radius*radius;
    }




    @Override
    public String toString(){

        return "radius Of Circle= "+radius;
    }
}